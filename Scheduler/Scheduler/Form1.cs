﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scheduler
{
    public partial class Form1 : Form
    {
        CourseList courses = new CourseList("Courses.csv");
        CandidateScheduleClass CandidateSchedule = new CandidateScheduleClass();
        CourseList searchResults = new CourseList();
        int startComboIndex = 0;
        int endComboIndex = 0;

        //variables to set the locations of the buttons
        const int horizontalOffset = 40;
        const int verticalOffset = 20;
        const int hourClassHeight = 59;
        const int classWidth = 48;
        const int dayWidth = classWidth * 2;

        public Form1()
        {
            InitializeComponent();
            courses.updatePrereq("prereqCourse.csv");
            searchResults = courses;

            for (int i = 0; i < searchResults.getNumCourses(); i++)
            {
                SearchGridView.Rows.Add(searchResults.getCourseCode(i), searchResults.getCourseShortName(i), searchResults.getDisplayTime(i));
            }

            // UNIT TESTING BLOCK:
            bool runUnitTests = false;// If this is changed to TRUE, a suite of unit tests will run for SearchCourseFiltered 

            if (runUnitTests == true)
            {
                Tests unitTests = new Tests();
                unitTests.unitTestSet(courses);
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            SearchGridView.Rows.Clear();

            string daysSelected = getDays();
            string startTime = StartTimeComboBox.Items[startComboIndex].ToString(); // Get the start and end time in data's 24 hour code (not the display form)
            string endTime = EndTimeComboBox.Items[endComboIndex].ToString();

            //get the search results
            searchResults = courses.searchCourseFiltered(SearchBar.Text, startTime, endTime, daysSelected, isPopularCourseFilterSelected());

            //add to the search results table
            for (int i = 0; i < searchResults.getNumCourses(); i++)
            {
                SearchGridView.Rows.Add(searchResults.getCourseCode(i), searchResults.getCourseShortName(i), searchResults.getDisplayTime(i));

                CourseSectionClass temp = searchResults.getCourse(i);

                foreach (timePair element in temp.times)
                {
                    for (int k = 0; k < element.meetDays.Count(); k++)
                    {
                        for (int j = 0; j < CandidateSchedule.candidateScheduleCourseList.Count; j++)
                        {
                            foreach (timePair CandidateElement in CandidateSchedule.candidateScheduleCourseList[j].Item1.times)
                            {
                                
                                if (Math.Max(element.getStartMilitaryTime(), CandidateElement.getStartMilitaryTime()) < Math.Min(element.getEndMilitaryTime(), CandidateElement.getEndMilitaryTime()) && (CandidateElement.meetDays.Contains(element.meetDays[k])))
                                {
                                    SearchGridView.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
                                }
                            }
                        }
                    }

                }
                
            }
        }

        private bool isPopularCourseFilterSelected()
        {
            if (popularCourseCheckBox.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string getDays()
        {
            string daysList = "";
            if (checkBox1.Checked)
            {
                daysList += "M";
            }
            if (checkBox2.Checked)
            {
                daysList += "T";
            }
            if (checkBox3.Checked)
            {
                daysList += "W";
            }
            if (checkBox4.Checked)
            {
                daysList += "R";
            }
            if (checkBox5.Checked)
            {
                daysList += "F";
            }
            return daysList;
        }

        private void SearchGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //unused
        }

        private void CandidateScheduleTableView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //unused
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //open the file dialog
            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.Filter = saveFile.Filter = "CSV (*.csv)|*.csv";
            saveFile.RestoreDirectory = true;

            try
            {
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    //save the file
                    CandidateSchedule.saveSchedule(saveFile.FileName);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Could not save file: " + ex);
            }

            saveFile.Dispose();

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CandidateScheduleTableView.Rows.Clear();

            ////clear the calender view
            ////go through the courses in the schedule
            //for (int i = 0; i < CandidateSchedule.getSize(); i++)
            //{
            //    CourseSectionClass coursePlaceholder = CandidateSchedule.getCourse(i);
            //    //remove all the buttons
            //    for (int j = 0; j < coursePlaceholder.listOfButtons.Count(); j++)
            //    {
            //        panel1.Controls.Remove(coursePlaceholder.listOfButtons[j]);
            //    }
            //}

            //open up the file dialog
            OpenFileDialog loadFile = new OpenFileDialog();

            //filter searches
            loadFile.Filter = "CSV (*.csv)|*.csv";
            loadFile.RestoreDirectory = true;

            try
            {
                if (loadFile.ShowDialog() == DialogResult.OK)
                {

                    //clear the candidate schedule
                    int numberOfClassesInCandidateScheduleCourseList = CandidateSchedule.candidateScheduleCourseList.Count;
                    for (int i = 0; i < numberOfClassesInCandidateScheduleCourseList; i++)
                    {
                        CourseSectionClass temp = CandidateSchedule.getCourse(0);


                        removeCourseFromCandidateSchedule(temp);
                    }

                    //read in all the information and store it in the candidate schedule variable
                    CandidateSchedule = CandidateSchedule.loadSchedule(loadFile.FileName);

                    foreach (MutableTuple<CourseSectionClass, int> course in CandidateSchedule.candidateScheduleCourseList)
                    {
                        addToCandidateScheduleAndGrid(course.Item1);
                    }

                    loadFile.Dispose();
                   
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Could not open file: " + ex);
            }
        }

        private void SearchBar_KeyUp(object sender, KeyEventArgs e)
        {
            // Unused: event whenever search bar has received a keystroke
        }

        public void button1_Click(object sender, EventArgs e)
        {
            //Unused
        }

        public void addToCandidateScheduleAndGrid(CourseSectionClass temp)
        {

            

            List<int> conflictIndices = new List<int>();
            int numMaxConflicts = getConflicts(temp, CandidateSchedule, out conflictIndices);

            // Do not add courses with more than 2 conflicting courses in a single day
            if (numMaxConflicts >= 1)
            {

                //give the user the chance to swap out conflicting courses.
                string message = "Adding this course would conflict with:\n";
                for (int i = 0; i < conflictIndices.Count; i++)
                {
                    message += CandidateSchedule.candidateScheduleCourseList[conflictIndices[i]].Item1.courseSectionCode;
                    message += "\n";
                }
                message += "\nRemove these course(s) to add the new course?";
                var result = MessageBox.Show(new Form {TopMost = true}, message, "Conflict!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                

                //if the user hit "yes", swap the courses.
                if (result == DialogResult.Yes)
                {
                    int numberOfConflictIndices = conflictIndices.Count;
                    int offset = 0;
                    for (int j = 0; j < numberOfConflictIndices; j++)
                    {
                        
                        removeCourseFromCandidateSchedule(CandidateSchedule.candidateScheduleCourseList[conflictIndices[j]-offset].Item1);
                        offset++;
                    }

                }
                else
                {
                    return;
                }

            }

            //add to candidate schedule
            CandidateSchedule.addCourse(temp);

            //add to candidate schedule table
            CandidateScheduleTableView.Rows.Add(temp.courseSectionCode, temp.courseShortName, temp.getDisplayTime());

            //add to calendar view here

            makeScheduleButtons(temp);
            // Add all buttons

            foreach (Button newButton in temp.listOfButtons)
            {
                panel1.Controls.Add(newButton);
                newButton.BringToFront();
            }

            for (int i = 0; i < searchResults.course_list.Count(); i++)
            {
                foreach (timePair element in temp.times)
                {
                    for (int k = 0; k < element.meetDays.Count(); k++)
                    {

                        foreach (timePair CandidateElement in searchResults.course_list[i].times)
                        {
                            if (Math.Max(element.getStartMilitaryTime(), CandidateElement.getStartMilitaryTime()) < Math.Min(element.getEndMilitaryTime(), CandidateElement.getEndMilitaryTime()) && (CandidateElement.meetDays.Contains(element.meetDays[k])))
                            {
                                SearchGridView.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
                            }
                        }

                    }

                }
            }
        }

        private bool makeScheduleButtons(CourseSectionClass course)
        {
            
            foreach (timePair element in course.times)
            {
                for (int i = 0; i < element.meetDays.Count(); i++)
                {
                    Button buttonPlaceholder = new Button();

                    Point newLoc = new Point(0, 0);

                    //find how tall the cell should be.
                    var component = (element.getIntFormEnd() - element.getIntFormStart()) / 60.0f;
                    var height = component * hourClassHeight;

                    //setting button properties
                    buttonPlaceholder.Size = new Size(classWidth * 2, (int)height);
                    buttonPlaceholder.BackColor = Color.LightSkyBlue;
                    buttonPlaceholder.Name = element.meetDays[i].ToString();
                    //buttonPlaceholder.Size = new Size(buttonPlaceholder.Size.Width*2, buttonPlaceholder.Size.Height);



                    buttonPlaceholder.Text = course.courseSectionCode;
                    //button click event handler. Display all course information
                    buttonPlaceholder.Click += (s, z) =>
                    {
                        PopupControl.Popup infoPopup = new PopupControl.Popup(new PopupControl1(this, courses, CandidateSchedule, course, course.courseSectionCode, course.courseLongName, course.getDisplayTime(), course.enrollment, course.capacity, course.building, course.room, course.prerequisite, false));
                        infoPopup.AutoClose = true;
                        infoPopup.Show(buttonPlaceholder);
                    };

                    //calculate where the button should go
                    switch (element.meetDays[i])
                    {
                        //wierd math is needed so that we don't round too soon.
                        case 'M':
                            var x = (element.getIntFormStart() - 480);
                            var y = x / 60.0f;
                            var z = y * hourClassHeight;
                            newLoc = new Point(label6.Location.X + horizontalOffset, label6.Location.Y + (int)(z));
                            break;
                        case 'T':
                            x = (element.getIntFormStart() - 480);
                            y = x / 60.0f;
                            z = y * hourClassHeight;
                            newLoc = new Point(label6.Location.X + horizontalOffset + dayWidth, label6.Location.Y + (int)(z));
                            break;
                        case 'W':
                            x = (element.getIntFormStart() - 480);
                            y = x / 60.0f;
                            z = y * hourClassHeight;
                            newLoc = new Point(label6.Location.X + horizontalOffset + dayWidth * 2, label6.Location.Y + (int)(z));
                            break;
                        case 'R':
                            x = (element.getIntFormStart() - 480);
                            y = x / 60.0f;
                            z = y * hourClassHeight;
                            newLoc = new Point(label6.Location.X + horizontalOffset + dayWidth * 3, label6.Location.Y + (int)(z));
                            break;
                        case 'F':
                            x = (element.getIntFormStart() - 480);
                            y = x / 60.0f;
                            z = y * hourClassHeight;
                            newLoc = new Point(label6.Location.X + horizontalOffset + dayWidth * 4, label6.Location.Y + (int)(z));
                            break;
                        default:
                            break;
                    }

                    buttonPlaceholder.Location = newLoc;
                    //label6
                    course.listOfButtons.Add(buttonPlaceholder);
                }
            }
            return true;
        }

        private void ScheduleViewRemoveButton_Click(object sender, EventArgs e)
        {
            //Removed
        }

        private void AdvFilterButton_Click(object sender, EventArgs e)
        {
            // Toggle visibility and clickability of the group
            AdvFilterGroup.Enabled = !AdvFilterGroup.Enabled;
            AdvFilterGroup.Visible = !AdvFilterGroup.Visible;
        }

        private void StartTimeComboBox_Format(object sender, ListControlConvertEventArgs e)
        {
            string tempString = e.Value.ToString();
            e.Value = timeFormat(tempString);
        }

        private void EndTimeComboBox_Format(object sender, ListControlConvertEventArgs e)
        {
            string tempString = e.Value.ToString();
            e.Value = timeFormat(tempString);
        }

        private string timeFormat(string timeString)
        {
            //parse the begin time and the end time
            if (timeString == "<None>")
            {
                return "<None>";
            }

            string[] TimeParsed = timeString.Split(':');
            //change the hours from military time

            int placeHolder = Int32.Parse(TimeParsed[0]);

            string meridian = "AM";

            if (placeHolder >= 12)
            {
                placeHolder = placeHolder % 12;
                meridian = "PM";
            }

            if (placeHolder == 0)
            {
                placeHolder = 12;
            }

            return placeHolder.ToString() + ":" + TimeParsed[1] + " " + meridian;
        }

        private void StartTimeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            startComboIndex = StartTimeComboBox.SelectedIndex;
        }

        private void EndTimeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            endComboIndex = EndTimeComboBox.SelectedIndex;
        }

        private void ClearFiltButton_Click(object sender, EventArgs e)
        {
            // Clearing all filters
            startComboIndex = 0;
            endComboIndex = 0;
            StartTimeComboBox.SelectedIndex = 0;
            EndTimeComboBox.SelectedIndex = 0;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;

            popularCourseCheckBox.Checked = false;

            // Re-run the search with the filters removed
            SearchButton_Click(sender, e);
        }

        private void SearchGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                CourseSectionClass temp = searchResults.getCourse(SearchGridView.CurrentCell.RowIndex);
                //popup
                PopupControl.Popup infoPopup = new PopupControl.Popup(new PopupControl1(this, searchResults, CandidateSchedule, temp, temp.courseSectionCode, temp.courseLongName, temp.getDisplayTime(), temp.enrollment, temp.capacity, temp.building, temp.room, temp.prerequisite, true));
                infoPopup.AutoClose = true;
                infoPopup.Show(SearchGridView, SearchGridView.Rows[e.RowIndex].Cells[0].ContentBounds.Left + 20, SearchGridView.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).Bottom);

            }
        }

        private void SearchGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //add course
            if (!(CandidateSchedule.isInSchedule(searchResults.getCourse(SearchGridView.CurrentCell.RowIndex))))
            {
                CourseSectionClass temp = searchResults.getCourse(SearchGridView.CurrentCell.RowIndex);

                //add all courses with the same course code
                CourseList sameCourseCode = courses.searchCourseCode(temp.courseSectionCode);

                //add to the table and the grid for all courses with the same name
                for (int i = 0; i < sameCourseCode.getNumCourses(); i++)
                {
                    addToCandidateScheduleAndGrid(sameCourseCode.getCourse(i));
                }
            }
        }

        private void CandidateScheduleTableView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //popup
                CourseSectionClass temp = CandidateSchedule.getCourse(CandidateScheduleTableView.CurrentCell.RowIndex);

                PopupControl.Popup infoPopup = new PopupControl.Popup(new PopupControl1(this, courses, CandidateSchedule, temp, temp.courseSectionCode, temp.courseLongName, temp.getDisplayTime(), temp.enrollment, temp.capacity, temp.building, temp.room, temp.prerequisite, false));
                infoPopup.AutoClose = true;
                infoPopup.Show(CandidateScheduleTableView, CandidateScheduleTableView.Rows[e.RowIndex].Cells[0].ContentBounds.Left + 20, CandidateScheduleTableView.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).Bottom);
            }
        }

        private void CandidateScheduleTableView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //delete from calendar view
            CourseSectionClass temp = CandidateSchedule.getCourse(CandidateScheduleTableView.CurrentCell.RowIndex);
            foreach (timePair element in temp.times)
            {
                for (int i = 0; i < element.meetDays.Count(); i++)
                {
                    panel1.Controls.Remove(temp.listOfButtons[i]);
                }
            }

            //remove from candidate schedule
            CandidateSchedule.removeCourse(CandidateSchedule.getCourse(CandidateScheduleTableView.CurrentCell.RowIndex));

            //delete from candidate table
            CandidateScheduleTableView.Rows.RemoveAt(CandidateScheduleTableView.CurrentCell.RowIndex);
        }

        public void removeCourseFromCandidateSchedule(CourseSectionClass temp)
        {
           
            // Remove any buttons
            for (int i = 0; i < temp.listOfButtons.Count(); i++)
            {
                panel1.Controls.Remove(temp.listOfButtons[i]);
            }

            temp.listOfButtons.Clear();

            //delete from candidate table
            int indexInSchedule = CandidateSchedule.findCourse(temp.courseSectionCode);
            CandidateScheduleTableView.Rows.RemoveAt(indexInSchedule);

            //remove from candidate schedule
            CandidateSchedule.removeCourse(temp);

            for (int i = 0; i < searchResults.course_list.Count(); i++)
            {
                SearchGridView.Rows[i].DefaultCellStyle.BackColor = Color.White;

                foreach (timePair element in searchResults.course_list[i].times)
                {
                    for (int k = 0; k < element.meetDays.Count(); k++)
                    {
                        for (int j = 0; j < CandidateSchedule.candidateScheduleCourseList.Count; j++)
                        {
                            foreach (timePair CandidateElement in CandidateSchedule.candidateScheduleCourseList[j].Item1.times)
                            {
                                //CandidateSchedule.candidateScheduleCourseList[j].Item1.courseSectionCode != temp.courseSectionCode && 
                                if (Math.Max(element.getStartMilitaryTime(), CandidateElement.getStartMilitaryTime()) < Math.Min(element.getEndMilitaryTime(), CandidateElement.getEndMilitaryTime()) && (CandidateElement.meetDays.Contains(element.meetDays[k])))
                                {
                                    SearchGridView.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
                                }

                            }
                        }
                    }

                }
            }


        }

        private void referenceNumbersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //display table with reference numbers
            Form2 refTable = new Form2(CandidateSchedule);
            refTable.Show();
        }

        private void ClearCandidateScheduleButton_Click(object sender, EventArgs e)
        {
            //clear the candidate schedule
            int numberOfClassesInCandidateScheduleCourseList = CandidateSchedule.candidateScheduleCourseList.Count;
            for (int i = 0; i < numberOfClassesInCandidateScheduleCourseList; i++)
            {
                CourseSectionClass temp = CandidateSchedule.getCourse(0);


                removeCourseFromCandidateSchedule(temp);
            }
        }

        // Return value is the maximum number of conflicts
        // conflictList is cleared and changed to the list of all candidate indices of courses that conflict
        int getConflicts(CourseSectionClass course, CandidateScheduleClass candidateList, out List<int> conflictList)
        {
            conflictList = new List<int>(); // Clear the list
            
            foreach (timePair courseTimes in course.times)
            {

                foreach (MutableTuple<CourseSectionClass, int> candidateCourse in candidateList.candidateScheduleCourseList)
                {
                    bool hasConflict = false;
                    foreach (char currday in courseTimes.meetDays)
                    {
                        
                        foreach (timePair candidateTimes in candidateCourse.Item1.times)
                        {
                            if (candidateCourse.Item1.courseSectionCode != course.courseSectionCode && Math.Max(courseTimes.getStartMilitaryTime(), candidateTimes.getStartMilitaryTime()) < Math.Min(courseTimes.getEndMilitaryTime(), candidateTimes.getEndMilitaryTime()) && (candidateTimes.meetDays.Contains(currday)))
                            {
                                hasConflict = true;
                                conflictList.Add(candidateList.findCourse(candidateCourse.Item1));
                                break;
                            }
                        }
                        if (hasConflict)
                        {
                            break;
                        }
                    
                    }
                }
            }
            return conflictList.Count;
        }

    }

}
