﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scheduler
{
    public partial class Form2 : Form
    {
        public Form2(CandidateScheduleClass schedule)
        {
            InitializeComponent();

            for (int i = 0; i < schedule.candidateScheduleCourseList.Count; i++)
            {
                PrereqTableView.Rows.Add(schedule.getCourse(i).courseSectionCode, schedule.getCourse(i).courseSectionReferenceNumber);
            }
        }
    }
}
