﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scheduler
{
    public class Tests
    {
        CourseList courses;
        List<List<string>> courseTest = new List<List<string>>();
        List<bool> results = new List<bool>();
        // The course test string lists contain all of the course section codes (with double space before the secion letter)
        // Courses in each list must appear in the same order 
        // List 0: all courses
        // List 1: courses containing "phil" in any code or name field
        // List 2: courses containing "470" as part of a course code
        // List 3: a single course with the reference number "10032"
        // List 4: courses with "principles" as part of a long title
        // List 5: courses with classes on Monday, Wednesday, Thursday, and Friday
        // List 6: courses with classes on Wednesday and Friday
        // List 7: courses with begin time 6, end time 8
        // List 8: course with invalid begin time, end time 9AM

        public void unitTestSet(CourseList courses)
        {
            this.courses = courses;
            // Create all lists of course codes to be compared against
            makeCourseTests();

            // Run all tests

            // Test 0: Ensure that all courses are found when no search term is entered
            testTerm("",courseTest[0]);
            // Tests 1-3: Ensure that search is case-insensitive
            testTerm("PHIL",courseTest[1]); 
            testTerm("phil", courseTest[1]);
            testTerm("pHiL", courseTest[1]);
            // Tests 4-7: Check that garbage data matches no courses
            testTerm("AAAAAAAAAAAAAAAAAAAAAA");
            testTerm("6235892352345079235");
            testTerm("bab 80.78 lol");
            testTerm("sohfodi comp asohfo");
            // Test 8: Ensure that the number-only part of a course code may match even in the absence of a department
            testTerm("470", courseTest[2]);
            // Test 9: The five-digit reference number should be unique
            testTerm("10553",courseTest[3]);
            // Tests 10-11: The term should match if it begins any word in the short/long title
            testTerm("Principles",courseTest[4]);
            testTerm("Princ",courseTest[4]);
            // Test 12: the term should not match if the string matches only the end of a short/long title
            testTerm("ciples");

            //test days

            testDays("MWRF", courseTest[5]); //test 13
            testDays("WF",courseTest[6]); //test 14
            testDays("1D");//should be empty

            //test times
            testTimes("invalid", "1.7"); //should be every course
            testTimes("18:00:00", "20:00:00", courseTest[7]); //17
            testTimes("invalid", "9:00:00", courseTest[8]); //18

            // Report all tests which failed
            report();
        }


        // Test that a term results in no courses
        private bool testTerm(string searchTerm)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered(searchTerm, "<None>", "<None>", "", false);

            // Fail if the number of courses if nonzero- a match was improperly found
            if (filteredCourses.getNumCourses() == 0)
            {
                results.Add(true);
                return true;
            }
            else
            {
                results.Add(false);
                return false;
            }
        }

        // Test that a term results in the same list of courses as the courselist
        private bool testTerm(string searchTerm, List<string> correctList)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered(searchTerm, "<None>", "<None>", "", false);

            // Fail immediately if sizes are not the same
            if (correctList.Count != filteredCourses.getNumCourses())
            {
                results.Add(false);
                return false;
            }

            // Fail if a course is not found
            for (int i = 0; i < filteredCourses.getNumCourses(); i++ )
            {
                if (filteredCourses.getCourseCode(i) != correctList[i])
                {
                    results.Add(false);
                    return false;
                }
            }
            results.Add(true);
            return true;
        }

        private bool testDays(string days)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered("", "<None>", "<None>", days, false);

            // Fail if the number of courses if nonzero- a match was improperly found
            if (filteredCourses.getNumCourses() == 0)
            {
                results.Add(true);
                return true;
            }
            else
            {
                results.Add(false);
                return false;
            }
        }

        private bool testDays(string days, List<string> correctList)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered("", "<None>", "<None>", days, false);

            // Fail immediately if sizes are not the same
            if (correctList.Count != filteredCourses.getNumCourses())
            {
                results.Add(false);
                return false;
            }

            // Fail if a course is not found
            for (int i = 0; i < filteredCourses.getNumCourses(); i++)
            {
                if (filteredCourses.getCourseCode(i) != correctList[i])
                {
                    results.Add(false);
                    return false;
                }
            }
            results.Add(true);
            return true;
        }

        private bool testTimes(string begin, string end)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered("", begin, end, "", false);

            // Fail if the number of courses if nonzero- a match was improperly found
            if (filteredCourses.getNumCourses() == 0)
            {
                results.Add(true);
                return true;
            }
            else
            {
                results.Add(false);
                return false;
            }
        }

        private bool testTimes(string begin, string end, List<string> correctList)
        {
            // Only filter by text entry
            CourseList filteredCourses = courses.searchCourseFiltered("", begin, end, "", false);

            // Fail immediately if sizes are not the same
            if (correctList.Count != filteredCourses.getNumCourses())
            {
                results.Add(false);
                return false;
            }

            // Fail if a course is not found
            for (int i = 0; i < filteredCourses.getNumCourses(); i++)
            {
                if (filteredCourses.getCourseCode(i) != correctList[i])
                {
                    results.Add(false);
                    return false;
                }
            }
            results.Add(true);
            return true;
        }

        // Create the test cases
        private void makeCourseTests()
        {

            List<string> test0 = new List<string>();
            foreach (CourseSectionClass element in courses.course_list)
            {
                test0.Add(element.courseSectionCode);
            }
            courseTest.Add(test0);

            List<string> test1 = new List<string>();
            test1.Add("PHIL 161  A");
            test1.Add("PHIL 201  A");
            test1.Add("PHIL 312  A");
            test1.Add("PHIL 336  A");
            test1.Add("PHIL 339  A");
            test1.Add("PHIL 360  A");
            courseTest.Add(test1);

            List<string> test2 = new List<string>();
            test2.Add("BIOL 470  A");
            test2.Add("BIOL 470  B");
            test2.Add("BIOL 470  C");
            test2.Add("EXER 470  A");
            test2.Add("MECE 470  A");
            test2.Add("RELI 470  A");
            courseTest.Add(test2);

            List<string> test3 = new List<string>();
            test3.Add("EDUC 324  A");
            courseTest.Add(test3);

            List<string> test4 = new List<string>();
            test4.Add("ACCT 202  A");
            test4.Add("ACCT 202  B");
            test4.Add("ACCT 202  C");
            test4.Add("ACCT 202  D");
            test4.Add("BUSA 204  A");
            test4.Add("BUSA 204  B");
            test4.Add("BUSA 204  C");
            test4.Add("BUSA 301  A");
            test4.Add("BUSA 301  B");
            test4.Add("BUSA 301  C");
            test4.Add("COMM 110  A");
            test4.Add("EXER 403  A");
            courseTest.Add(test4);

            List<string> test5 = new List<string>();
            test5.Add("MATH 161  B");
            courseTest.Add(test5);

            List<string> test6 = new List<string>();
            test6.Add("CHEM 406  A");
            test6.Add("EXER 223  A");
            test6.Add("EXER 251  A"); 
            test6.Add("EXER 305  A");
            test6.Add("EXER 306  B");
            test6.Add("MECE 452  P");
            test6.Add("PHYE 102  B");
            test6.Add("PHYE 112  C");
            test6.Add("PHYE 201  A");
            test6.Add("PHYE 205  A");
            test6.Add("PHYE 207  B");
            test6.Add("PHYE 209  A");
            test6.Add("PHYE 217  A");
            test6.Add("PHYE 220  A");
            test6.Add("PHYE 221  A");
            test6.Add("SPAN 362  A");
            courseTest.Add(test6);

            List<string> test7 = new List<string>();
            test7.Add("ENTR 467  A");
            test7.Add("MUSI 111  B");
            test7.Add("POLS 277  A");
            test7.Add("PSYC 105  A");
            test7.Add("SOCI 105  A");
            courseTest.Add(test7);

            List<string> test8 = new List<string>();
            test8.Add("ACCT 202  A");
            test8.Add("BIOL 102  A");
            test8.Add("BIOL 314  A");
            test8.Add("BIOL 320  A");
            test8.Add("BUSA 201  A");
            test8.Add("BUSA 214  A");
            test8.Add("BUSA 301  A");
            test8.Add("BUSA 304  A");
            test8.Add("CHEM 242  A");
            test8.Add("CHEM 463  A");
            test8.Add("COMM 135  A");
            test8.Add("EDUC 204  A");
            test8.Add("ELEE 304  A");
            test8.Add("ENGL 202  A");
            test8.Add("ENGR 402  A");
            test8.Add("EXER 233  A");
            test8.Add("EXER 251  A");
            test8.Add("EXER 256  A");
            test8.Add("HUMA 102  A");
            test8.Add("HUMA 201  A");
            test8.Add("HUMA 301  A");
            test8.Add("MATH 162  A");
            test8.Add("MATH 303  A");
            test8.Add("MECE 210  A");
            test8.Add("MECE 326  A");
            test8.Add("MUSI 107  A");
            test8.Add("PHYE 102  A");
            test8.Add("PHYE 201  A");
            test8.Add("PHYE 216  A");
            test8.Add("PHYE 216  B");
            test8.Add("PHYS 102  A");
            test8.Add("PHYS 122  A");
            test8.Add("POLS 277  A");
            test8.Add("PSYC 101  A");
            test8.Add("SOCI 251  A");
            test8.Add("SPAN 306  A");
            test8.Add("SPAN 362  A");
            test8.Add("WRIT 101  A");
            courseTest.Add(test8);

        }

        private void report()
        {
            string messagestring = "The following test(s) have failed:\n";
            int initLength = messagestring.Length;
            for (int i = 0; i < results.Count; i++ )
            {
                if (results[i] == false)
                {
                    messagestring += i + ", "; // Add each failed test index
                }
            }
            if (messagestring.Length == initLength)
            {
                messagestring += "None!"; // If no test failure reported
            }
            else
            {
                messagestring = messagestring.Remove((messagestring.Length - 2)); // Remove last comma and space
            }
            MessageBox.Show(messagestring);
        }
    }

}