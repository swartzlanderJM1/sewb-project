Important notes:

Our .csv files are named Courses.csv, and prereqCourse.csv. These need to be in the programs access directory. This should be taken care of but are making you aware just in case.
The save and load feature is under the file menu along with the Reference number table option.

The unit tests run when Line 41 is set to true, and our current file has it at false. This will run seperate unitTests which will result in a popup with the results of the tests.
The file that contains the unit tests is unitTests.cs. It tests the functionality of the searchCourseFiltered function which returns the search results. 

Instructions to build and run project:
Open up Visual Studio via the Project Directory
Run with or without using debugging
No extra steps needed.
Enjoy an easy way to make your schedule.

